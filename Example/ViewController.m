//
//  ViewController.m
//  Example
//
//  Created by Michael Gunawan on 12/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import "ViewController.h"
#import <test_splitio_framework/SplitIOService.h>

@interface ViewController ()
@property (strong, nonatomic) SplitIOService *splitService;
@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.splitService = [[SplitIOService alloc]initWithAPIKey:@"" matchingKey:@"user_test"];
	[self getData];
}

- (IBAction)buddonClicked:(UIButton *)sender {
    [self getData];
}

- (void)getData {
	[self.splitService getDatawithSplitNames:@"first_split"
								  attributes:@{@"merchant_name" : @"kfc"}
							 completionBlock:^(NSDictionary * _Nonnull result) {
		        self.label.text = result[@"setting_feedback"][@"title"];
				self.textView.text = result.descriptionInStringsFileFormat;
	}
									 onError:^(NSError * _Nonnull error) {
		//do stuff
	}];
}

@end
