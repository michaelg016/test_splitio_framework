//
//  ViewController.h
//  Example
//
//  Created by Michael Gunawan on 12/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textBox;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UITextView *textView;


@end

