//
//  SplitIOService.m
//  test_splitio_framework
//
//  Created by Michael Gunawan on 12/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import "SplitIOService.h"
@import Split;

@interface SplitIOService()

//@property (strong, nonatomic) id<SplitClient> client;
//@property (strong, nonatomic) id<SplitManager> manager;
@property (strong, nonatomic) id<SplitFactoryBuilder> builder;
@property (assign, nonatomic) Boolean isReady;
@property (strong, nonatomic) NSString * _Nonnull apiKey;
@property (strong, nonatomic) NSString * _Nonnull matchingKey;
@end

@implementation SplitIOService
- (instancetype)init {
    self = [super init];
    self.isReady = NO;
    self.apiKey = @"";
    self.matchingKey = @"";
    [self prepareSplit];
    return self;
}

- (nullable instancetype)initWithAPIKey:(nonnull NSString *)apiKey matchingKey:(nonnull NSString *)matchingKey {
	self = [super init];
	self.apiKey = apiKey;
    self.matchingKey = matchingKey;
	[self prepareSplit];
	return self;
}

- (void)prepareSplit {
    // Split Config
    SplitClientConfig *config = [[SplitClientConfig alloc] init];
    config.featuresRefreshRate = 30;
    config.segmentsRefreshRate = 30;
    config.impressionRefreshRate = 30;
    config.sdkReadyTimeOut = 15000;
    config.connectionTimeout = 50;
    
    //User Key
    Key *key = [[Key alloc] initWithMatchingKey:self.matchingKey bucketingKey:nil];
    
    //Split FactoryBuilder
    self.builder = [[DefaultSplitFactoryBuilder alloc] init];
	[_builder setApiKey: self.apiKey];
	[_builder setKey: key];
	[_builder setConfig: config];
}

- (void)getDatawithSplitNames:(NSString * _Nonnull)splitName attributes:(NSDictionary * _Nonnull)attributes completionBlock:(void (^)(NSDictionary * _Nonnull))completionBlock onError:(void (^)(NSError * _Nonnull))errorBlock {
	//Split Factory
	id<SplitFactory> factory = [_builder build];
	id<SplitClient> client = factory.client;
	[client onEvent: SplitEventSdkReady execute: ^(){
		SplitResult *result = [client getTreatmentWithConfig:splitName attributes: attributes];
		NSMutableDictionary<NSString *, id> *resultDict = [[NSMutableDictionary alloc] init];
		[resultDict setValue:result.treatment forKey:@"status"];
		if (result.config) {
			NSData *webData = [result.config dataUsingEncoding:NSUTF8StringEncoding];
			NSError *error;
			NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:webData options:0 error:&error];
			for(int i = 0;i < jsonDict.allKeys.count ; i++) {
				NSData *tempData = [jsonDict[jsonDict.allKeys[i]] dataUsingEncoding:NSUTF8StringEncoding];
				[resultDict setValue:[NSJSONSerialization JSONObjectWithData:tempData options:0 error:&error] forKey:jsonDict.allKeys[i]];
			}
		}
		[client flush];
		[client destroy];
		completionBlock(resultDict);
	}];
	
}
@end
