//
//  SplitIOService.h
//  test_splitio_framework
//
//  Created by Michael Gunawan on 12/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface SplitIOService : NSObject
- (nullable instancetype)initWithAPIKey:(nonnull NSString *)apiKey matchingKey:(nonnull NSString *)matchingKey;
- (void)getDatawithSplitNames:(NSString * _Nonnull)SplitName attributes:(NSDictionary * _Nonnull)attributes completionBlock:(void (^)(NSDictionary * _Nonnull))completionBlock onError:(void (^)(NSError * _Nonnull))errorBlock;
@end

NS_ASSUME_NONNULL_END
